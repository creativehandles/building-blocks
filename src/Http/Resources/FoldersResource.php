<?php

namespace Creativehandles\BuildingBlocks\Http\Resources;

use App\Http\Resources\CoreJsonResource;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FoldersResource extends CoreJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "over_folder_id" => $this->over_folder_id,
            "name" => $this->name,
            "overfolder" => new OverFoldersResource($this->whenLoaded("overFolder")),
            "blocks" => BlocksResource::collection($this->whenLoaded("blocks")),
        ];
    }
}
