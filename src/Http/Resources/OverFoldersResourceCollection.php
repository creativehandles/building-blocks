<?php

namespace Creativehandles\BuildingBlocks\Http\Resources;

use App\Http\Resources\BaseResourceCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OverFoldersResourceCollection extends BaseResourceCollection
{

    public function getResourceClass(): string
     {
       return  OverFoldersResource::class;
     }
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
