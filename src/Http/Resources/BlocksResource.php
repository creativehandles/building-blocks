<?php

namespace Creativehandles\BuildingBlocks\Http\Resources;

use App\Http\Resources\CoreJsonResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Resources\RelatedImageResource;

class BlocksResource extends CoreJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "folder_id" => $this->folder_id,
            "name" => $this->name,
            "short_description" => $this->short_description,
            "main_description" => $this->main_description,
            "additional_description" => $this->additional_description,
            // "image_url" => $this->getMediaMatchAll(['image_url-'.app()->getLocale()])->first() ? $this->getMediaMatchAll(['image_url-'.app()->getLocale()])->first()->getUrl() : "",
            // "image_full_url" => $this->getMediaMatchAll(['image_url-'.app()->getLocale()])->first() ? $this->getMediaMatchAll(['image_url-'.app()->getLocale()])->first()->getUrl() : "",
            "image_url" => url($this->image_url),
            "image_path" => $this->image_url,
            "images" => RelatedImageResource::collection($this->whenLoaded("images")),
            "folder"=> new FoldersResource($this->whenLoaded("folder"))
        ];
    }
}
