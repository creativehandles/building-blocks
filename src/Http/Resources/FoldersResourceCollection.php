<?php

namespace Creativehandles\BuildingBlocks\Http\Resources;

use App\Http\Resources\BaseResourceCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class FoldersResourceCollection extends BaseResourceCollection
{

    public function getResourceClass(): string
     {
       return  FoldersResource::class;
     }
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
