<?php

namespace Creativehandles\BuildingBlocks\Http\Resources;

use App\Http\Resources\CoreJsonResource;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OverFoldersResource extends CoreJsonResource
{
/**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name"=>$this->name,
            "folders"=> FoldersResource::collection($this->whenLoaded("folders"))
            
        ];
    }

}
