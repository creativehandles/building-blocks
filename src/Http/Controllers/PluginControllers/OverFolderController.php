<?php

namespace Creativehandles\BuildingBlocks\Http\Controllers\PluginControllers;

use App\Http\Controllers\BaseControllers\BasePluginController;
use App\Traits\MediaManagerTrait;
use Creativehandles\BuildingBlocks\Repositories\BlocksRepository;
use Creativehandles\BuildingBlocks\Repositories\OverFoldersRepository;
use Creativehandles\BuildingBlocks\Services\BuildingBlocksService;
use Exception;
use Illuminate\Http\Request;

class OverFolderController extends BasePluginController
{
    use MediaManagerTrait;

    public function getService()
    {
        return app(BuildingBlocksService::class);
    }

    public function getRepository()
    {
        return app(OverFoldersRepository::class);
    }

    public function getViewsFolder($override = null)
    {
        return "Admin.BuildingBlocks.OverFolders.";
    }

    public function getIndexRoute()
    {
        return "admin.overfolders.index";
    }

    public function getCreateRoute()
    {
        return "admin.overfolders.create";
    }

    public function getEditRoute()
    {
        return "admin.overfolders.edit";
    }

    public function getDeleteRoute()
    {
        return "admin.overfolders.delete";
    }

    public function getDestroyRoute()
    {
        return "admin.overfolders.destroy";
    }

    /** add the name of the resource as in routes
     * ex : Route::resource('admin/overfolders', 'overfoldersController')->name('overfolders');
     */
    public function getResourceName()
    {
        return "overfolders";
    }

    public function validateRequest(Request $request)
    {
       $request->validate([
            'name' => "required",
        ]);
    }

    public function create()
    {
        $items = $this->repository->all();
        return view($this->viewsFolder . 'add')->with('items', $items);
    }

    public function edit($id)
    {

        $item = $this->repository->find($id);

        $categories = $this->repository->all();

        if (empty($item)) {
            \Session::flash("error", __('Item not found'));

            return redirect(route($this->indexRoute));
        }

        return view($this->viewsFolder . 'add')
            ->with('model', $item)->with('item', $item);
    }

    public function grid(Request $request)
    {
        try {

            $searchColumns = ['ch_over_folders.id', 'ch_over_folders.title'];
            $orderColumns = ['1' => 'ch_over_folders.id'];
            $with = [];
            $data = $this->repository->getDataforDataTables($searchColumns, $orderColumns, '1', $with);
            $translateBtns = "";
            $forgetBtns = "";

            $data['data'] = $data['data']->map(function ($item) use ($translateBtns, $forgetBtns) {
                $hasChildren = (string) ($item->folders->count());
                $return = [
                    $item->id,
                    $item->name ,
                    ($hasChildren ?  $hasChildren. " ".__('general.Folder/s') : "N/A" ). $this->filteredFolders($item->id),
                    $this->translateButton($item, $this->getEditRoute(), ['overfolder' => $item->id]),
                    $this->editButton($item, $this->getEditRoute(), ['overfolder' => $item->id]),
                    $this->deleteButton($item, $this->getDestroyRoute()),
                ];

                return $return;
            });

            return json_encode($data);
        } catch (Exception $e) {
            return response()->json(['msg' => ['Error occurred'], 'developer_msg' => $e->getMessage() . $e->getTraceAsString()], 500);
        }
    }


    public function filteredFolders($overFolderId = null){

        return ' <a href="'.route(app(FolderController::class)->getIndexRoute())."?overfolder=".$overFolderId.'"><i class="fa fa-folder-open"></i></a>';

    }
}
