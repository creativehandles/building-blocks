<?php

namespace Creativehandles\BuildingBlocks\Http\Controllers\PluginControllers;

use App\Http\Controllers\BaseControllers\BasePluginController;
use App\Traits\MediaManagerTrait;
use Creativehandles\BuildingBlocks\Repositories\BlocksRepository;
use Creativehandles\BuildingBlocks\Services\BuildingBlocksService;
use Exception;
use Illuminate\Http\Request;

class BlockController extends BasePluginController
{
    use MediaManagerTrait;

    public function getService()
    {
        return app(BuildingBlocksService::class);
    }

    public function getRepository()
    {
        return app(BlocksRepository::class);
    }

    public function getViewsFolder($override = null)
    {
        return "Admin.BuildingBlocks.Blocks.";
    }

    public function getIndexRoute()
    {
        return "admin.blocks.index";
    }

    public function getCreateRoute()
    {
        return "admin.blocks.create";
    }

    public function getEditRoute()
    {
        return "admin.blocks.edit";
    }

    public function getDeleteRoute()
    {
        return "admin.blocks.delete";
    }

    public function getDestroyRoute()
    {
        return "admin.blocks.destroy";
    }

    /** add the name of the resource as in routes
     * ex : Route::resource('admin/blocks', 'blocksController')->name('blocks');
     */
    public function getResourceName()
    {
        return "blocks";
    }

    public function validateRequest(Request $request)
    {
        $request->validate([
            'name' => "required",
        ]);
    }

    public function create()
    {
        $items = $this->repository->all();
        $folders = $this->service->getAllFolders();
        return view($this->viewsFolder . 'add')->with('items',$items)->with('folders',$folders);
    }

    public function edit($id)
    {

        $item = $this->repository->find($id);

        $folders = $this->service->getAllFolders();

        if (empty($item)) {
            \Session::flash("error", __('Item not found'));

            return redirect(route($this->indexRoute));
        }

        return view($this->viewsFolder . 'add')
        ->with('model', $item)->with('item',$item)->with('folders',$folders);
    }

    public function grid(Request $request)
    {
        try {

            $searchColumns = ['ch_blocks.id', 'ch_blocks.title'];
            $orderColumns = ['1' => 'ch_blocks.id'];
            $with = [];
            $where = $request->get('where',null);
            $data = $this->repository->getDataforDataTables($searchColumns, $orderColumns, '1', $with,$where);
            $translateBtns = "";
            $forgetBtns = "";

            $data['data'] = $data['data']->map(function ($item) use ($translateBtns, $forgetBtns) {

                $return = [
                    $item->id,
                    $item->name,
                    $item->folder->name,
                    // $item->status,
                    $this->translateButton($item, $this->getEditRoute(), ['block' => $item->id]),
                    $this->editButton($item, $this->getEditRoute(), ['block' => $item->id]),
                    $this->deleteButton($item, $this->getDestroyRoute()),
                ];

                return $return;
            });

            return json_encode($data);
        } catch (Exception $e) {
            return response()->json(['msg' => ['Error occurred'], 'developer_msg' => $e->getMessage() . $e->getTraceAsString()], 500);
        }
    }

    /**
     * Update the specified Items in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validateRequest($request);

        try {
            $formatData = $request->except(['multi_attachments', 'multiImageList']);

            if ($id) {
                $formatData['id'] = $id;
            }

            $items = $this->repository->updateOrCreate($formatData, 'id');


            //process main image for post
            // if ($request->has('image_url')) {
            //     $media = $this->processOne($request, 'image_url');
            //     $items->syncMedia($media, ['image_url-'. app()->getLocale()]);
            // }


            $imageList = $request->get('multiImageList', []); //here we are getting an array of images

            if ($imageList) {
                //link images
                if (count($imageList) > 0) {
                    $this->processImages($imageList, $items);
                } else {
                    $items->images()->where('model', $this->model)->delete();
                }
            }
        } catch (Exception $e) {
            if ($request->ajax()) {
                return response()->json(['msg' => ['Error occurred'], 'developer_msg' => $e->getMessage()], 500);
            } else {
                return redirect()->back()->withInput()->with('error', $e->getMessage())->withInput();
            }
        }

        if ($request->ajax()) {
            return response()->json(['msg' => __('Item updated successfully.'), 'data' => $items], 200);
        } else {
            \Session::flash("success", __('Item updated successfully.'));
            return redirect(route($this->indexRoute));
        }
    }
}
