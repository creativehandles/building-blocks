<?php

namespace Creativehandles\BuildingBlocks\Http\Controllers\PluginControllers;

use App\Http\Controllers\BaseControllers\BasePluginController;
use App\Traits\MediaManagerTrait;
use Creativehandles\BuildingBlocks\Repositories\FoldersRepository;
use Creativehandles\BuildingBlocks\Services\BuildingBlocksService;
use Exception;
use Illuminate\Http\Request;
use League\CommonMark\Parser\Block\BlockContinue;

class FolderController extends BasePluginController
{
    use MediaManagerTrait;

    public function getService()
    {
        return app(BuildingBlocksService::class);
    }

    public function getRepository()
    {
        return app(FoldersRepository::class);
    }

    public function getViewsFolder($override = null)
    {
        return "Admin.BuildingBlocks.Folders.";
    }

    public function getIndexRoute()
    {
        return "admin.folders.index";
    }

    public function getCreateRoute()
    {
        return "admin.folders.create";
    }

    public function getEditRoute()
    {
        return "admin.folders.edit";
    }

    public function getDeleteRoute()
    {
        return "admin.folders.delete";
    }

    public function getDestroyRoute()
    {
        return "admin.folders.destroy";
    }

    /** add the name of the resource as in routes
     * ex : Route::resource('admin/blocks', 'blocksController')->name('blocks');
     */
    public function getResourceName()
    {
        return "folders";
    }

    public function validateRequest(Request $request)
    {
        $request->validate([
            'name' => "required",
        ]);
    }

    public function create()
    {

        $items = $this->repository->all();
        $overfolders= $this->service->getAllOverFolders();
        return view($this->viewsFolder . 'add')->with('items', $items)->with('overfolders',$overfolders);
    }

    public function edit($id)
    {

        $item = $this->repository->find($id);

        $overfolders= $this->service->getAllOverFolders();

        if (empty($item)) {
            \Session::flash("error", __('Item not found'));

            return redirect(route($this->indexRoute));
        }

        return view($this->viewsFolder . 'add')
            ->with('model', $item)->with('item', $item)->with('overfolders',$overfolders);
    }

    public function grid(Request $request)
    {
        try {

            $searchColumns = ['ch_folders.id', 'ch_folders.title'];
            $orderColumns = ['1' => 'ch_folders.id'];
            $with = [];
            $where = $request->get('where',null);
            $data = $this->repository->getDataforDataTables($searchColumns, $orderColumns, '1', $with,$where);
            $translateBtns = "";
            $forgetBtns = "";

            $data['data'] = $data['data']->map(function ($item) use ($translateBtns, $forgetBtns) {

                $hasChildren = (string) ($item->blocks->count());

                $return = [
                    $item->id,
                    $item->name . ($hasChildren ? " <span class='fa fa-level-down' title='Has childrens'><span> " : ""),
                    ($hasChildren ?  $hasChildren. " ". __('general.Block/s') : " N/A"). $this->filteredFolders($item->id),
                    $this->translateButton($item, $this->getEditRoute(), ['folder' => $item->id]),
                    $this->editButton($item, $this->getEditRoute(), ['folder' => $item->id]),
                    $this->deleteButton($item, $this->getDestroyRoute()),
                ];

                return $return;
            });

            return json_encode($data);
        } catch (Exception $e) {
            return response()->json(['msg' => ['Error occurred'], 'developer_msg' => $e->getMessage() . $e->getTraceAsString()], 500);
        }
    }

    public function filteredFolders($folderId = null){

        return ' <a href="'.route(app(BlockController::class)->getIndexRoute())."?folder=".$folderId.'"><i class="fa fa-folder-open"></i></a>';

    }
}
