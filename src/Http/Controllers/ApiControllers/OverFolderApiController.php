<?php

namespace Creativehandles\BuildingBlocks\Http\Controllers\ApiControllers;

use App\Http\Controllers\BaseControllers\BaseApiController;
use Creativehandles\BuildingBlocks\Repositories\OverFoldersRepository;

class OverFolderApiController extends BaseApiController{

    public function getRepository()
    {
        return app(OverFoldersRepository::class);
    }

    //prepare list of default loading relationships ex: return ["tags","categories"]
    public function getDefaultRelations()
    {
        return [];
    }

    //prepare list of orderable columns ex: return ["title","short_description","main_description","additional_description"]
    public function getSearchableColumns()
    {
        return ['name'];
    }

    //prepare list of orderable columns ex: return ["ch_posts.id"=>"id","ch_posts.title"=>"title"]
    public function getOrderableColumns()
    {
        return ['ch_over_folders.id'=>"id"];
    }

}