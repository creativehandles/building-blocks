<?php

namespace Creativehandles\BuildingBlocks\Repositories;

use App\Repositories\BaseEloquentRepository;
use Creativehandles\BuildingBlocks\Http\Resources\FoldersResource;
use Creativehandles\BuildingBlocks\Http\Resources\FoldersResourceCollection;
use Creativehandles\BuildingBlocks\Models\Folder;

class FoldersRepository extends BaseEloquentRepository{


    public function getModel()
    {
        return new Folder();
    }

    public function getResource()
    {
        return FoldersResource::class;
    }

    public function getResourceCollection(){
        return FoldersResourceCollection::class;
    }

    public function getDataforDataTables(array $searchColumns = null, array $orderColumns = null, string $defOrderColumnKey = null, array $with = null, array $where= null){
        return parent::getDataforDataTables($searchColumns,$orderColumns,$defOrderColumnKey,$with,$where);
    }


    public function getDataIndexforDataTables($collection)
    {
        return $collection;
    }
}
