<?php

namespace Creativehandles\BuildingBlocks\Repositories;

use App\Repositories\BaseEloquentRepository;
use Creativehandles\BuildingBlocks\Http\Resources\OverFoldersResource;
use Creativehandles\BuildingBlocks\Http\Resources\OverFoldersResourceCollection;
use Creativehandles\BuildingBlocks\Models\OverFolder;

class OverFoldersRepository extends BaseEloquentRepository{


    public function getModel()
    {
        return new OverFolder();
    }

    public function getResource()
    {
        return OverFoldersResource::class;
    }

    public function getResourceCollection(){
        return OverFoldersResourceCollection::class;
    }


    public function getDataforDataTables(array $searchColumns = null, array $orderColumns = null, string $defOrderColumnKey = null, array $with = null, array $where= null){
        return parent::getDataforDataTables($searchColumns,$orderColumns,$defOrderColumnKey,$with,$where);
    }


    public function getDataIndexforDataTables($collection)
    {
        return $collection;
    }
}
