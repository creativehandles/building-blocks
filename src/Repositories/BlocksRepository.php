<?php

namespace Creativehandles\BuildingBlocks\Repositories;

use App\Repositories\BaseEloquentRepository;
use Creativehandles\BuildingBlocks\Http\Resources\BlocksResource;
use Creativehandles\BuildingBlocks\Http\Resources\BlocksResourceCollection;
use Creativehandles\BuildingBlocks\Models\Block;

class BlocksRepository extends BaseEloquentRepository{


    public function getModel()
    {
        return new Block();
    }

    public function getResource()
    {
        return BlocksResource::class;
    }

    public function getResourceCollection(){
        return BlocksResourceCollection::class;
    }

    public function getDataforDataTables(array $searchColumns = null, array $orderColumns = null, string $defOrderColumnKey = null, array $with = null, array $where= null){
        return parent::getDataforDataTables($searchColumns,$orderColumns,$defOrderColumnKey,$with,$where);
    }


    public function getDataIndexforDataTables($collection)
    {
        return $collection;
    }
}
