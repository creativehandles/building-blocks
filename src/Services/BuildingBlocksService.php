<?php

namespace Creativehandles\BuildingBlocks\Services;

use Creativehandles\BuildingBlocks\Repositories\BlocksRepository;
use Creativehandles\BuildingBlocks\Repositories\FoldersRepository;
use Creativehandles\BuildingBlocks\Repositories\OverFoldersRepository;

class BuildingBlocksService
{

    public $blocksRepository;

    public $foldersRepository;

    public $overFoldersRepository;


    public function __construct(BlocksRepository $blocksRepository, FoldersRepository $foldersRepository, OverFoldersRepository $overFoldersRepository)
    {
        $this->blocksRepository = $blocksRepository;
        $this->foldersRepository = $foldersRepository;
        $this->overFoldersRepository = $overFoldersRepository;
    }

    public function getAllOverFolders()
    {
        return $this->overFoldersRepository->all();
    }

    public function getAllFolders()
    {
        return $this->foldersRepository->all();
    }
}
