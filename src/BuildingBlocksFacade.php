<?php

namespace Creativehandles\BuildingBlocks;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Creativehandles\BuildingBlocks\Skeleton\SkeletonClass
 */
class BuildingBlocksFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'building-blocks';
    }
}
