<?php

namespace Creativehandles\BuildingBlocks;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class BuildingBlocksServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'building-blocks');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'building-blocks');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');



        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('building-blocks.php'),
            ], 'config');

            //publishing migrations
            $this->publishes([
                __DIR__ . '/../database/migrations/create_ch_over_folders_table.php' => database_path('migrations/' . date('Y_m_d_His', Carbon::now()->addSeconds(1)->timestamp) . '_create_ch_over_folders_table.php'),
                __DIR__ . '/../database/migrations/create_ch_folders_table.php' => database_path('migrations/' . date('Y_m_d_His', Carbon::now()->addSeconds(5)->timestamp) . '_create_ch_folders_table.php'),
                __DIR__ . '/../database/migrations/create_ch_blocks_table.php' => database_path('migrations/' . date('Y_m_d_His', Carbon::now()->addSeconds(10)->timestamp) . '_create_ch_blocks_table.php'),
                // you can add any number of migrations here
            ], 'migrations');


            // Publishing the views.
            $this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/Admin/BuildingBlocks/'),
            ], 'views');

            // Publishing assets.
            /*$this->publishes([
                __DIR__.'/../resources/assets' => public_path('vendor/building-blocks'),
            ], 'assets');*/

             // Publishing  controller.
             $this->publishes([
                __DIR__.'/../app/Http/Controllers/' => app_path('/Http/Controllers/'),
            ], 'pluginController');

            // Publishing the translation files.
            $this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/'),
            ], 'lang');

            // Registering package commands.
            // $this->commands([]);
            $this->publishes([
                __DIR__.'/../routes/web.php' => base_path('routes/packages/package-routes/building-blocks-web-routes.php'),
                __DIR__.'/../routes/api.php' => base_path('routes/packages/api-routes/building-blocks-api-routes.php'),
                __DIR__.'/../routes/breadcrumbs' => base_path('routes/packages/breadcrumbs/'),
            ], 'routes');
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'building-blocks');

        // Register the main class to use with the facade
        $this->app->singleton('building-blocks', function () {
            return new BuildingBlocks;
        });
    }
}
