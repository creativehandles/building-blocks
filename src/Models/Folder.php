<?php

namespace Creativehandles\BuildingBlocks\Models;

use App\Traits\ExtendedHasTranslationTrait;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Folder extends Model
{
    use Mediable;
    use ExtendedHasTranslationTrait;

    protected $table="ch_folders";

    protected $guarded = [];
    protected $hidden = [
            "deleted_by",
            "deleted_at"
    ];

    public $translatable=[
        "name",
    ];

    public function overFolder(){
        return $this->belongsTo(OverFolder::class,'over_folder_id','id');
    }

    public function blocks(){
        return $this->hasMany(Block::class,'folder_id','id');
    }

}
