<?php

namespace Creativehandles\BuildingBlocks\Models;

use App\Traits\ExtendedHasTranslationTrait;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class OverFolder extends Model
{
    use Mediable;
    use ExtendedHasTranslationTrait;

    protected $table="ch_over_folders";

    protected $guarded = [];
    protected $hidden = [
            "deleted_by",
            "deleted_at"
    ];

    public $translatable=[
        "name",
    ];

    public function folders(){
        return $this->hasMany(Folder::class,'over_folder_id','id');
    }
}
