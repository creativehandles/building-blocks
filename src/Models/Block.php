<?php

namespace Creativehandles\BuildingBlocks\Models;

use App\Traits\ExtendedHasTranslationTrait;
use App\Traits\HasImages;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Block extends Model
{
    use Mediable;
    use ExtendedHasTranslationTrait;
    use HasImages;

    protected $table="ch_blocks";

    protected $guarded = [];

    public $translatable=[
        "name",
        "short_description",
        "main_description",
        "additional_description",
    ];

    public function folder(){
        return $this->belongsTo(Folder::class,'folder_id','id');
    }
}
