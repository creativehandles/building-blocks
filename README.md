# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/creativehandles/building-blocks.svg?style=flat-square)](https://packagist.org/packages/creativehandles/building-blocks)
[![Total Downloads](https://img.shields.io/packagist/dt/creativehandles/building-blocks.svg?style=flat-square)](https://packagist.org/packages/creativehandles/building-blocks)
![GitHub Actions](https://github.com/creativehandles/building-blocks/actions/workflows/main.yml/badge.svg)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require creativehandles/building-blocks
```

## Usage
Publish required files (migrations,routes,views etc) to CORE CMS
```php
php artisan vendor:publish --provider="Creativehandles\BuildingBlocks\BuildingBlocksServiceProvider"


php artisan vendor:publish --provider="Creativehandles\ChBlocks\ChBlocksServiceProvider"

php artisan vendor:publish --provider="Creativehandles\ChGallery\ChGalleryServiceProvider"

```

Migrate database tables
```
php artisan migrate
```

### Testing

```bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email deemantha@creativehandles.com instead of using the issue tracker.

## Credits

-   [Deemantha](https://github.com/creativehandles)
-   [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).
