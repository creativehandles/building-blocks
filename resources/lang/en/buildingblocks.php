<?php

return [


    "Blocks"=>"Blocks",
    "Folder content"=>"Folder content",
    "Block content"=>"Block content",
    "id"=>"Id",
    "folder"=>"Folder",
    "name"=>"Name",
    "status"=>"Status",
    "Folders"=>"Folders",
    "blocks"=>"Blocks",
    "Over Folders"=>"Over Folders",
    "Over Folder content"=>"Over Folder content",
    "folders"=>"Folders",
    "Create new"=>"Create new",
    "Update"=>"Update",

    "More Information"=>"More Information"
];