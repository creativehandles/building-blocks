<?php

return [

    "Blocks" => "Bloky",
    "Folder content" => "Informace",
    "Block content" => "Blok obsah",
    "id" => "ID",
    "folder" => "Složka",
    "name" => "Titulek",
    "status" => "Stav",
    "Folders" => "Složky",
    "blocks" => "Bloky",
    "Over Folders" => "Přes složky",
    "Over Folder content" => "Informace",
    "folders" => "Složky",
    "Create new" => "Vytvořit novou",
    "Update" => "Aktualizovat",

    "More Information"=>"Další informace"
];
