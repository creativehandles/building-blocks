@extends('Admin.layout')
@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>
@endsection
@section('content')
@include('Admin.partials.form-alert')
@include('Admin.partials.breadcumbs',['header'=>__('buildingblocks.Over Folders')])
<div class="content-body">
    @if(isset($model))
    <form action="{{ route('admin.overfolders.update',['overfolder'=>$model->id]) }}" method="POST"
          class="form-horizontal" enctype="multipart/form-data">
        @else
            <form action="{{route('admin.overfolders.store')}}" method="POST"
                  class="form-horizontal" enctype="multipart/form-data">
        @endif

    <div class="row">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">
                    @if (isset($model))
                        <h4 class="card-title" id="basic-layout-form-center">{{__('general.Update')}} ({{request()->get('translating_lang', app()->getLocale())}})</h4>
                    @else
                        <h4 class="card-title" id="basic-layout-form-center">{{__('general.Create new')}}</h4>
                    @endif
                </div>

                    <div class="card-body">
                        <div class="row">
                        <div class="col-md-8 col-sm-12 border-right-blue-grey border-right-lighten-5">


                            {{ csrf_field() }}

                            @if (isset($model))

                                <input type="hidden" name="_method" value="PATCH">

                                        <input type="hidden" name="id" id="id" class="form-control" readonly="readonly" value="{{$model->id}}">
                                @else
                                        <input type="hidden" name="id" id="id" class="form-control" readonly="readonly" value="">
                            @endif
                            <h4 class="form-section"><i class="fa fa-tags"></i>{{ __("buildingblocks.Over Folder content") }}</h4>
                            <div class="form-group">
                                <label for="name" class="col-sm-12 control-label required">{{__('buildingblocks.name')}}</label>
                                <div class="col-sm-12">
                                    <input required type="text" name="name" id="name" class="form-control" value="{{isset($model) ? $model->name : (old('name'))?? '' }}">
                                </div>
                            </div>
                                @component('Admin.components.form-actions',['model'=>isset($model) ? $model : null])
                                @endcomponent
                            </div>

                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</div>


@endsection
@section('scripts')

{{-- @stack('component-scripts') --}}
@endsection


