<div class="form-group">
    <label for="over_folder_id" class="col-sm-12 control-label">{{__('general.Over Folder')}}</label>
    <div class="col-sm-12">
        <select  name="over_folder_id" id="over_folder_id" data-placeholder="{{__('general.select a category')}}" class="select2 form-control">
            @if(isset($overfolders))
                @foreach ($overfolders as $overfolder)
                    <option value="{{ $overfolder->id }}"  {{ isset($model) ? (in_array($overfolder->id,[$model->overfolder->id])) ? 'selected' : '' : ''}} >{{ $overfolder->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
