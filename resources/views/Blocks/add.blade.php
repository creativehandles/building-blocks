@extends('Admin.layout')
@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
    @include('Admin.partials.form-alert')
    @include('Admin.partials.breadcumbs', ['header' => __('buildingblocks.Blocks')])
    <div class="content-body">
        @if (isset($model))
            <form action="{{ route('admin.blocks.update', ['block' => $model->id]) }}" method="POST" class="form-horizontal"
                enctype="multipart/form-data">
            @else
                <form action="{{ route('admin.blocks.store') }}" method="POST" class="form-horizontal"
                    enctype="multipart/form-data">
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">
                        @if (isset($model))
                            <h4 class="card-title" id="basic-layout-form-center">{{ __('general.Update') }}
                                ({{ request()->get('translating_lang', app()->getLocale()) }})</h4>
                        @else
                            <h4 class="card-title" id="basic-layout-form-center">{{ __('general.Create new') }}</h4>
                        @endif
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 border-right-blue-grey border-right-lighten-5">

                                {{ csrf_field() }}

                                @if (isset($model))
                                    <input type="hidden" name="_method" value="PATCH">

                                    <input type="hidden" name="id" id="id" class="form-control"
                                        readonly="readonly" value="{{ $model->id }}">
                                @else
                                    <input type="hidden" name="id" id="id" class="form-control"
                                        readonly="readonly" value="">
                                @endif
                                <h4 class="form-section"><i class="fa fa-tags"></i>{{ __('buildingblocks.Folder content') }}
                                </h4>
                                <div class="form-group">
                                    <label for="name"
                                        class="col-sm-12 control-label">{{ __('buildingblocks.name') }}</label>
                                    <div class="col-sm-12">
                                        <input required type="text" name="name" id="name" class="form-control"
                                            value="{{ isset($model) ? $model->name : old('name') ?? '' }}">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="short_description"
                                        class="col-sm-12 control-label">{{ __('posts.short_description') }}</label>
                                    <div class="col-sm-12">
                                        <textarea rows="5" required name="short_description" id="short_description" class="form-control">{{ isset($model) ? $model->short_description : old('short_description') ?? '' }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="main_description"
                                        class="col-sm-12 control-label">{{ __('posts.main_description') }}</label>
                                    <div class="col-sm-12">
                                        <textarea required name="main_description" id="main_description" class="form-control summernote-body">{{ isset($model) ? $model->main_description : old('main_description') ?? '' }}</textarea>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="additional_description"
                                        class="col-sm-12 control-label">{{ __('posts.additional_description') }}</label>
                                    <div class="col-sm-12">
                                        <textarea name="additional_description" id="additional_description" class="form-control summernote-body">{{ isset($model) ? $model->additional_description : old('additional_description') ?? '' }}</textarea>
                                    </div>
                                </div>



                                @component('Admin.components.form-actions', ['model' => isset($model) ? $model : null])
                                @endcomponent
                            </div>
                            <div class="col-md-6 col-sm-12 border-right-blue-grey border-right-lighten-5">
                                <h4 class="form-section"><i class="fa fa-tags"></i>{{ __('buildingblocks.More Information') }}
                                </h4>
                                @component('Admin.BuildingBlocks.Folders.components.folder-selector',
                                ['model' => isset($model) ? $model : null, 'filedName' => 'folder_id', 'folders' => $folders])
                                @endcomponent

                                {{-- @component('Admin.components.single-image-type', ['model' => isset($model) ? $model : null, 'filedName' => 'image_url'])
                                @endcomponent --}}
                                @component('Admin.components.single-image-picker',
                                    ['model' => isset($model) ? $model : null, 'filedName' => 'single_attachment'])
                                @endcomponent

                                @component('Admin.components.multiple-images', ['model' => isset($model) ? $model : null])
                                @endcomponent

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    </div>
@endsection
@section('scripts')
    {{-- @push('component-scripts')
<script></script>
@endpush --}}
@endsection
