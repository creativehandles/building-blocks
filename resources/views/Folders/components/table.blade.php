<div class="row">
    <div class="col-md-12 col-12 mb-2">
        <div class="mb-2 pull-left">
            @component("Admin.components.language-picker")
           @endcomponent
        </div>
        <div class="mb-2 pull-right">
            <a href="{{route('admin.folders.create')}}" class="btn btn-secondary btn-block-sm"><i class="ft-file-plus"></i> {{__('general.Create new')}}</a>
            <a href="{{  route('admin.overfolders.index')  }}">
                <button type="button" href="" class="btn btn-warning mr-1">
                    <i class="ft-arrow-left"></i> {{__('trainings.general.goBack')}}
                </button></a>
        </div>

    </div>
</div>

<div class="table-responsive">
    <table class="table table-bordered table-striped full-width" id="thegrid">
        <thead>
        <tr>
            <th>{{__('buildingblocks.id')}}</th>
            <th>{{__('buildingblocks.name')}}</th>
            <th>{{__('buildingblocks.blocks')}}</th>
            <th class="action-col">{{__('general.Translate')}}</th>
            <th class="action-col" >{{__('general.Update')}}</th>
            <th class="action-col" >{{__('general.Delete')}}</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
