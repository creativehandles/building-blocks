<div class="form-group">
    <label for="folder_id" class="col-sm-12 control-label">{{__('general.Folder')}}</label>
    <div class="col-sm-12">
        <select  name="folder_id" id="folder_id" data-placeholder="select a folder" class="select2 form-control">
            @if(isset($folders))
                @foreach ($folders as $folder)
                    <option value="{{ $folder->id }}"  {{ isset($model) ? (in_array($folder->id,[$model->folder->id])) ? 'selected' : '' : ''}} >{{ $folder->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
