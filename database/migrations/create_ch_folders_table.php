<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChFoldersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ch_folders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('over_folder_id')->unsigned()->index();
            $table->json('name')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('deleted_by')->nullable()->default(NULL);
            $table->softDeletes(); $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ch_folders');
    }
}
