<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ch_blocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('folder_id')->unsigned()->index();
            $table->json('name')->nullable();
            $table->longText('image_url')->nullable();
            $table->json('short_description')->nullable();
            $table->json('main_description')->nullable();
            $table->json('additional_description')->nullable();
            $table->tinyInteger('status')->nullable()->default(0);
            $table->timestamps();
            $table->string('deleted_by')->nullable()->default(NULL);
            $table->softDeletes();
        });


        $enMenu = \DB::table('admin_menus')->where('name','Sidebar-en')->first();
        
        if(!$enMenu){
            \DB::table('admin_menus')->insert(
                        ['name'=>'Sidebar-en']
            );
        }
        $enMenu = \DB::table('admin_menus')->where('name','Sidebar-en')->first();
        if($enMenu){
            \DB::table('admin_menu_items')->insert([
                'label'=>"Building Blocks",
                "link"=>"admin.overfolders.index",
                "menu"=> $enMenu->id
            ]);
        }

        $csMenu = \DB::table('admin_menus')->where('name','Sidebar-cs')->first();
        
        if(!$csMenu){
            \DB::table('admin_menus')->insert(
                        ['name'=>'Sidebar-cs']
            );
        }
        $csMenu = \DB::table('admin_menus')->where('name','Sidebar-cs')->first();
        if($csMenu){
            \DB::table('admin_menu_items')->insert([
                'label'=>"Stavební bloky",
                "link"=>"admin.overfolders.index",
                "menu"=> $csMenu->id
            ]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ch_blocks');
    }
}
