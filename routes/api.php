<?php

use Illuminate\Support\Facades\Route;

Route::group(['name' => 'Blocks API', 'groupName' => 'Blocks API','prefix'=>'plugins'], function () {
    Route::resource('blocks', 'PackageControllers\BlockApiResourceController')->only(['index','show']);
    Route::resource('folders', 'PackageControllers\FolderApiResourceController')->only(['index','show']);
    Route::resource('overfolders', 'PackageControllers\OverFolderApiResourceController')->only(['index','show']);
});

