<?php

/** overfolders BreadCrumbs */
// Dashboard > overfolders
Breadcrumbs::for('admin.overfolders.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push(__('buildingblocks.Over Folders'), route('admin.overfolders.index'));
});

// Dashboard > overfolders > create
Breadcrumbs::for('admin.overfolders.create', function ($trail) {
    $trail->parent('admin.overfolders.index');
    $trail->push(__('buildingblocks.Create new'), route('admin.overfolders.create'));
});

// Dashboard > overfolders > edit
Breadcrumbs::for('admin.overfolders.edit', function ($trail, $item) {
    $trail->parent('admin.overfolders.index');
    $trail->push(__('buildingblocks.Update'), route('admin.overfolders.edit', $item->item ?? ''));
});


/** folders BreadCrumbs */
// Dashboard > folders
Breadcrumbs::for('admin.folders.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push(__('buildingblocks.Folders'), route('admin.folders.index'));
});

// Dashboard > folders > create
Breadcrumbs::for('admin.folders.create', function ($trail) {
    $trail->parent('admin.folders.index');
    $trail->push(__('buildingblocks.Create new'), route('admin.folders.create'));
});

// Dashboard > folders > edit
Breadcrumbs::for('admin.folders.edit', function ($trail, $item) {
    $trail->parent('admin.folders.index');
    $trail->push(__('buildingblocks.Update'), route('admin.folders.edit', $item->item ?? ''));
});


/** blocks BreadCrumbs */
// Dashboard > blocks
Breadcrumbs::for('admin.blocks.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push(__('buildingblocks.Blocks'), route('admin.blocks.index'));
});

// Dashboard > blocks > create
Breadcrumbs::for('admin.blocks.create', function ($trail) {
    $trail->parent('admin.blocks.index');
    $trail->push(__('buildingblocks.Create new'), route('admin.blocks.create'));
});

// Dashboard > blocks > edit
Breadcrumbs::for('admin.blocks.edit', function ($trail, $item) {
    $trail->parent('admin.blocks.index');
    $trail->push(__('buildingblocks.Update'), route('admin.blocks.edit', $item->item ?? ''));
});
