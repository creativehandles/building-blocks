<?php

use Illuminate\Support\Facades\Route;


Route::group(['name' => 'Building Blocks', 'groupName' => 'Building Blocks','prefix'=>'building-blocks'], function () {
    //blocks related routes
    Route::get('overfolders/grid', 'PackageControllers\OverFolderCrudController@grid')->name('overfolders.grid');
    Route::resource('overfolders', 'PackageControllers\OverFolderCrudController');

    Route::get('folders/grid', 'PackageControllers\FolderCrudController@grid')->name('folders.grid');
    Route::resource('folders', 'PackageControllers\FolderCrudController');

    Route::get('blocks/grid', 'PackageControllers\BlockCrudController@grid')->name('blocks.grid');
    Route::resource('blocks', 'PackageControllers\BlockCrudController');
});
