<?php

namespace App\Http\Controllers\PackageControllers;

use Creativehandles\BlogPosts\Http\Controllers\ApiControllers\PostApiController;
use Creativehandles\BuildingBlocks\Http\Controllers\ApiControllers\BlockApiController;
use Illuminate\Http\Request;

/**
 * @group Blocks
 *
 * APIs for Blocks
 */
class BlockApiResourceController extends BlockApiController
{
    /**
     * Get all Blocks
     *
     * @queryParam search string Query to search. you can search through title or description. Example: Lorem
     * @queryParam order[0][column] string  Order column  Possible values("id")
     * @queryParam order[0][dir] string  Possible values(asc,desc)
     *
     *
     * @queryParam where[0][column]
     * If u are using multiple where query params, it will support to AND operator in mysql
     * Possible values (id,name)
     *
     *
     * Possible where types
     *  ["table.column","relationship:column_checking"].
     *
     *
     * NOTE: above items with ":" are for relationships. relationship:column_checking
     * ex categories:title => you are looking for category name in categories relationship
     * Possible Values (folder:name).
     *
     *
     *
     *
     * @queryParam where[0][operator] Possible operators (=,>,<,>=,<=,like) if u are using like make sure to add % properly in the value as it is mysql operators. Example: like
     *
     * @queryParam where[0][value] Value you are looking for
     *
     *
     * @queryParam with[0]  Load relationships . Example: categories
     * @queryParam with[1]  Load relationships. Example: tags
     * @queryParam with[2]  Load relationships
     * possible values (folder,folder.overFolder,images). Example: images
     *
     * @queryParam withoutRelations boolean drop all relations.
     *
     * Possible values (true)
     *
     *
     * @responseFile responses/blocks-all.200.json
     *
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        return parent::index($request);
    }

    /**
     * Get single block by id
     *
     *
     * @urlParam id integer required Id of the block.
     *
     * @queryParam with[0]  Load relationships . Example: folder
     * @queryParam with[1]  Load relationships
     * possible values (folder,folder.overFolder,images)
     *
     *
     * @responseFile responses/block-single.200.json
     */
    public function show(Request $request, $id)
    {
        return parent::show($request, $id);
    }
}
