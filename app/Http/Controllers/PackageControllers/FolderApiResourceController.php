<?php

namespace App\Http\Controllers\PackageControllers;

use Creativehandles\BuildingBlocks\Http\Controllers\ApiControllers\FolderApiController;
use Illuminate\Http\Request;

/**
 * @group Blocks
 *
 * APIs for Blocks
 */
class FolderApiResourceController extends FolderApiController
{
     /**
     * Get all Folders
     *
     * @queryParam search string Query to search. you can search through title or description. Example: Lorem
     * @queryParam order[0][column] string  Order column  Possible values("id")
     * @queryParam order[0][dir] string  Possible values(asc,desc)
     *
     *
     * @queryParam where[0][column]
     * If u are using multiple where query params, it will support to AND operator in mysql
     * Possible values (id,name)
     *
     *
     * Possible where types
     *  ["table.column","relationship:column_checking"].
     *
     *
     * NOTE: above items with ":" are for relationships. relationship:column_checking
     * ex categories:title => you are looking for category name in categories relationship
     * Possible Values (blocks:name,overFolder:name).
     *
     *
     *
     *
     * @queryParam where[0][operator] Possible operators (=,>,<,>=,<=,like) if u are using like make sure to add % properly in the value as it is mysql operators. Example: like
     *
     * @queryParam where[0][value] Value you are looking for
     *
     *
     * @queryParam with[0]  Load relationships . Example: categories
     * @queryParam with[1]  Load relationships. Example: tags
     * @queryParam with[2]  Load relationships
     * possible values (blocks,overFolder). Example: images
     *
     * @queryParam withoutRelations boolean drop all relations.
     *
     * Possible values (true)
     *
     *
     * @responseFile responses/folder-all.200.json
     *
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        return parent::index($request);
    }

    /**
     * Get single overfolder by id
     *
     *
     * @urlParam id integer required Id of the folder.
     *
     * @queryParam with[0]  Load relationships . Example: categories
     * @queryParam with[1]  Load relationships
     * possible values (overFolder,blocks)
     *
     *
     * @responseFile responses/folder-single.200.json
     */
    public function show(Request $request, $id)
    {
        return parent::show($request, $id);
    }
}
